#Репозиторий курса DevOps от Нетологии

Автор Горшков Леонид Валерьевич

Добавлено ДЗ 1 по лекции "Введение в DevOps"

[01-intro-01](https://github.com/dzhangrLV/devops-netology/tree/main/01-Intro-01)

Добавлено ДЗ 2 по лекции "Системы контроля версий"

[02-git-01](https://github.com/dzhangrLV/devops-netology/tree/main/02-git-01)

Добавлено ДЗ 3 по лекции "Основы Git"

[02-git-02](https://github.com/dzhangrLV/devops-netology/tree/main/02-git-02)

